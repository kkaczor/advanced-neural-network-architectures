# Advanced Neural Network Architectures

Combined papers:

M1 - https://paperswithcode.com/paper/mixnet-mixed-depthwise-convolutional-kernels

M2 - https://paperswithcode.com/paper/visual-attention-network

M3 - https://paperswithcode.com/paper/filter-response-normalization-layer